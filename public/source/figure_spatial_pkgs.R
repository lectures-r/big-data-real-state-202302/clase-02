
# library
if(!require(pacman)){install.packages(pacman) ; require(pacman)}
p_load(rvest,dplyr,stringr,cranlogs,data.table,ggplot2,zoo)

# url packages
html_spatial = read_html("https://cran.r-project.org/web/views/Spatial.html")

# get packages names
packages = html_spatial %>% 
           html_nodes(xpath = "/html/body/ul[1]") %>% 
           html_children() %>% 
           html_text() %>% 
           str_replace_all("\\(core\\)", "") %>% 
           str_trim()

# get downloads packages
db = lapply(2000:2020 , function(x) cran_downloads(packages=packages , from=paste0(x,"-01-01"), to=paste0(x,"-12-31"))) %>% 
     rbindlist(use.names = T , fill = T)

# group data
df = db %>% 
     mutate(month = substr(date,1,7)) %>% 
     group_by(package,month) %>% 
     summarize(average = mean(count)) %>%
     subset(package %in% c("spatial","sp","sf","raster","stars","ggmap")) %>%
     mutate(date = as.yearmon(month)) %>% subset(date>2012&date<2020)

ggplot() + 
geom_line(data=df , aes(x=date ,y =average , color=package) , size=1) + 
labs(color="" , x="Fecha" , y="Descargas promedio por día") + 
scale_y_continuous(expand = c(0,0)) + 
scale_x_yearmon(expand = c(0,0) , n=10) +
theme_light() + 
theme(axis.title = element_text(size=20) , 
      axis.text = element_text(size=12) , 
      legend.text = element_text(size=20) , 
      legend.position="right") + 
scale_colour_manual(values=c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7"))
ggsave(filename="public/pics/download_packages.png" , width=9 , height=4.5)


