---
pagetitle: "Clase 02: Modelo de equilibrio espacial"
title: "**Big Data and Machine Learning en el Mercado Inmobiliario**"
subtitle: "**Clase 02:** Modelo de equilibrio espacial"
author: 
  name: Eduard Fernando Martínez González
  affiliation: Universidad de los Andes | [BD-ML](https://ignaciomsarmiento.github.io/teaching/BDML)
# date: Lecture 10  #"`r format(Sys.time(), '%d %B %Y')`"
output: 
  html_document:
    theme: flatly
    highlight: haddock
    # code_folding: show
    toc: yes
    toc_depth: 4
    toc_float: yes
    keep_md: false
    keep_tex: false ## Change to true if want keep intermediate .tex file
    ## For multi-col environments
  pdf_document:
    latex_engine: xelatex
    toc: true
    dev: cairo_pdf
    # fig_width: 7 ## Optional: Set default PDF figure width
    # fig_height: 6 ## Optional: Set default PDF figure height
    includes:
      in_header: tex/preamble.tex ## For multi-col environments
    pandoc_args:
        --template=tex/mytemplate.tex ## For affiliation field. See: https://bit.ly/2T191uZ
always_allow_html: true
urlcolor: blue
mainfont: cochineal
sansfont: Fira Sans
monofont: Fira Code ## Although, see: https://tex.stackexchange.com/q/294362
## Automatically knit to both formats
---

```{r setup, include=F , cache=F}
# load packages
require(pacman)
p_load(here,knitr,tidyverse,ggthemes,fontawesome,kableExtra)
p_load(tidyverse,rio,viridis,sf, leaflet, tmaptools)

# option html
options(htmltools.dir.version = F)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)
```

<!------------------------->
<!--- Datos geográficos --->
<!------------------------->
## **[1.] Datos geográficos**

<!---------------->
#### **1.1. ¿Qué es GIS?**

Un **Sistema de Información Geográfico** (GIS por sus siglas en inglés), es un sistema (software + hardware) que permite gestionar y analizar la información capturada en el entorno físico.

![](pics/gis_intro.png){width=600}

Ver código de R [aquí](https://gitlab.com/lectures-r/big-data-real-state-202202/clase-02/-/blob/main/public/source/figure_intro_gis.R)

<!---------------->
#### **1.2. Modelos de datos geográficos**

##### **1.2.1. Datos vectoriales:**

La geometría de este tipo de datos se compone de uno o más vértices interconectados. Un vértice describe una posición en el espacio utilizando un eje X y un eje Y (opcionalmente también un eje z). Mas información [aquí](https://docs.qgis.org/2.14/es/docs/gentle_gis_introduction/vector_data.html).

##### **1.2.2. Datos raster:**

Un conjunto de datos ráster está compuesto por una matriz de píxeles (también conocidos como celdas). Cada píxel representa una región geográfica, y el valor del píxel representa alguna característica de la región. Mas información [aquí](https://docs.qgis.org/2.14/es/docs/gentle_gis_introduction/raster_data.html).

<!---------------->
#### **1.3. Geometrías para datos vectoriales**

![](pics/geometry.png){width=600}

Tomado de: [Applied Spatial Data Analysis with R](https://www.springer.com/gp/book/9781461476177)   
**Punto:** está compuesto de un solo vértice.
**Línea o arco:** está compuesto de dos o más vértices, pero el primer y el último vértice deben ser diferentes. **Polígono:** está compuesto de tres o más vértices, pero el último vértice es igual a la primero.

<!---------------->
#### **1.4. Extensiones de los archivos**

```{r,echo=F}
my_tbl = tribble(
~Nombre,~Extensión,~Tipo,
" ESRI Shapefile",".shp (archivo principal)","Vector",
" GeoJSON",".geojson","Vector",
" KML",".kml","Vector",
" GPX",".gpx","Vector",
" GeoTIFF",".tif/.tiff","Raster",
" Arc ASCII",".asc","Raster",
" R-raster",".gri,.grd","Raster",
" SQLite/SpatiaLite",".sqlite","Vector y raster",
" ESRI FileGDB",".gdb","Vector y raster",
" GeoPackage",".gpkg","Vector y raster")

kbl(my_tbl) %>%
kable_paper("striped", full_width = F) %>%
row_spec(0, bold = T, color = "white", background = "#00008B")
```

Nota: Además del archivo **.shp** (que almacena la geometría del objeto), el ESRI Shapefile debe contener un archivo **.shx** (almacena el índice de la geometría), un archivo **.dbf** (dataframe con los atributos) y un archivo **.prj** (contiene información del CRS).

<!--------------------------------------------------->
<!--- Sistemas de Referencia de Coordenadas (CRS) --->
<!--------------------------------------------------->
## **[2.] Sistemas de Referencia de Coordenadas - CRS**

Todo objeto espacial está asociado a un CRS (Coordinate Reference Systems). El CRS proporciona una referencia espacial estandarizada de un objeto. El CRS puedes ser **Sistema de Coordenadas Geográficas** o un **Sistema de Coordenadas Proyectadas**.

#### **2.1. Sistema de Coordenadas Geográficas - SCG**

En este SCG las ubicaciones se miden en unidades angulares desde el centro de la tierra en relación con dos planos (uno definido en el ecuador y otro definido en el primer meridiano). En este sentido una ubicación se define como la combinación entre un valor de latitud (-90:90) y un valor de longitud (-180:180).

#### **2.2. Sistema de Coordenadas Proyectadas - SCP**

Es un sistema de referencia de coordenadas bidimensional, se define normalmente mediante dos ejes. Ubicados en ángulo recto uno respecto al otro, formando el denominado plano XY (como en un plano cartesiano). Existen por lo menos 3 familias de Sistemas de Coordenadas Proyectadas, Plana (**a**),  Cónica (**b**) o Cilíndrica (**c**). 

![](pics/projection_families.png){width=400}

Tomado de: [https://docs.qgis.org](https://docs.qgis.org/2.8/en/docs/gentle_gis_introduction/coordinate_reference_systems.html)

Por ejemplo, los CRS geográficos y los CRS proyectados están expresados en unidades diferentes. El mapa de la izquierda tiene un CRS que está representado en grados decimales, mientras que la unidad de medida del mapa de la derecha es metros.

![](pics/geographic_projected.png){width=600}

Ver código de R [aquí](https://gitlab.com/lectures-r/big-data-real-state-202202/clase-02/-/blob/main/public/source/figure_csr.R)

Sim embargo, dentro de los CRS geográficos/proyectadas pueden existir puntos de origen diferentes. El mapa de la derecha usa un CRS con [EPSG](https://epsg.io) **NAD83** y tiene las unidades de medida en metros, el de la derecha también tiene unidades de medidas de metros, pero usa una CRS Azimuthal que está centrada en Tegucigalpa (coordenadas `lon_0=-87` y `lat_0=14`):

![](pics/origen.png){width=600}

Ver código de R [aquí](https://gitlab.com/lectures-r/big-data-real-state-202202/clase-02/-/blob/main/public/source/figure_csr.R)

#### **2.3. Código EPSG**

Todas las CRS cuentan con un código de la European Petroleum Survey Group (EPSG), el cual almacena los parámetros geodésicos con códigos estándar. Puede encontrar otras referencias de EPSG [aquí](https://epsg.io)

<!------------------>
<!--- R-espacial --->
<!------------------>
## **[3.] R-espacial**

[Aquí](https://github.com/r-spatial) puede acceder a los repositorios de GitHub con las librerías de [r-spatial](https://github.com/r-spatial).

<!---------------->
#### **3.1. Popularidad de R-espacial**

![](pics/download_packages.png){width=450}

Ver código de R [aquí](https://gitlab.com/lectures-r/big-data-real-state-202202/clase-02/-/blob/main/public/source/figure_spatial_pkgs.R)

<!---------------->
#### **3.2. Librería sf**

Fue escrita por [Edzer Pebesma](https://github.com/edzer) y [Roger Bivand](https://github.com/rsbivand) quienes también la librería `sp` (la versión anterior de `sf`).  

##### **¿Por qué cambiar a `sf` si `sp` ya ha sido probada y probada? **

• Lectura y escritura rápida de datos.

• Rendimiento de trazado mejorado.

• Los objetos sf pueden tratarse como dataframes en la mayoría de las operaciones.

• Las funciones de sf se pueden combinar con el operador %>% y trabaja bastante bien con las funciones de la librería `tidyverse`.

• Los nombres de las funciones sf son relativamente consistentes e intuitivos (todos comienzan con st_).

<!---------------->
##### **Documentación:**

```{r,eval=T,echo=T}
vignette(package = "sf") # Ver viñetas disponibles
```
```{r,eval=F,echo=T}
Vignettes in package ‘sf’:

sf1                      1. Simple Features for R (source, html)
sf2                      2. Reading, Writing and Converting Simple
                         Features (source, html)
sf3                      3. Manipulating Simple Feature Geometries
                         (source, html)
sf4                      4. Manipulating Simple Features (source, html)
sf5                      5. Plotting Simple Features (source, html)
sf6                      6. Miscellaneous (source, html)
sf7                      7. Spherical geometry in sf using s2geometry
                         (source, html)
```

<!---------------->
#### **3.3. ¿Qué es un simple feature?**

Un simple feature es cualquier objeto en el mundo real, como una calle o un edificio. Un objeto `sf` posee una geometría (describe lugar de la Tierra en el que se encuentra) y unos atributos (describe otras propiedades del objeto). La geometría de un árbol puede ser el un círculo con el diámetro de su copa o un punto que indica su centro. Las propiedades pueden incluir su altura, color, diámetro, etc.

<div><img src="https://docs.qgis.org/2.14/es/_images/feature_at_glance.png" height=380></div>
Tomado de: [https://docs.qgis.org](https://docs.qgis.org)   

<!---------------->
##### **Tipos de geometrías para `sf`**

Un objeto `sf`  puede almacenar por lo menos seis de los siguientes tipos de datos vectoriales: 

![](pics/type_geometry.png){width=600}

<!---------------->
##### **Objeto sf en R**

Por ejemplo, si usted tiene un objeto de clase `sf` al que le asignó el nombre de `spatial_objetc`. Puede inspeccionar la clase del objeto así:

```{r,eval=F}
class(spatial_objetc)
"sf"         "data.frame"
```

Y al imprimirlo sobre la consola puede observar que este objeto contiene atributos (columnas `DPTO_CCDGO`,`MPIO_CCDGO`,`CPOB_CCDGO`,`NANG_RTCN` y `CSIMBOL`) e información sobre la geometría:

![](pics/sf_console.png){width=600}
<!--==================-->
<!--==================-->
## **[4.] Modelo monocéntrico**

Para replicar este ejercicio, primero debe descargar el siguiente [proyecto de R](https://gitlab.com/lectures-r/big-data-real-state-202302/clase-02/-/archive/main/clase-02-main.zip?path=clase_02) y abrir el archivo `clase-02.Rproj`. Ahora puede seguir el script de la clase que está ubicado en: `source/clase-02.R`

#### **4.1 Configuración inicial**

```{r}
## llamar pacman (contiene la función p_load)
require(pacman)

## llamar y/o instalar librerias
p_load(tidyverse,rio,
       viridis, ## paletas de colores 
       sf, ## datos espaciales
       leaflet, ## visualizaciones
       tmaptools, ## geocodificar
       ggsn, ## map scale bar 
       tidycensus) ## packages with census data
```

#### **4.2 Bostón y Chicago**

Puede obtener la API-key de **US Census Bureau Data** [aquí](http://api.census.gov/data/key_signup.html). Además, puede obtener una documentación de la librería [aquí](https://walker-data.com/tidycensus/articles/basic-usage.html).

```{r, eval=F}
census_api_key("YOUR CENSUS API HERE")
```

```{r,eval=F}
## get Median Housing Values Cook county
chicago = get_acs(geography="block group",variables="B25077_001E",state ="IL",county="Cook County",year=2016,geometry=T)

## get Median Housing Values Suffolk County
boston = get_acs(geography="block group",variables="B25077_001E",state ="MA",county="Suffolk County",year=2016,geometry = T)
```
```{r,echo=F,message=F,warning=F,results=F}
## get Median Housing Values Cook county
chicago = get_acs(geography="block group",variables="B25077_001E",state ="IL",county="Cook County",year=2016,geometry=T)

## get Median Housing Values Suffolk County
boston = get_acs(geography="block group",variables="B25077_001E",state ="MA",county="Suffolk County",year=2016,geometry = T)
```
```{r,fig.height=1.5,fig.width=2.5}
## clase del objeto
class(chicago)

## plot data
leaflet(boston) %>% addTiles() %>% addPolygons(color="green",fill=NA,weight=2)

## Boston CBD
boston_cbd = geocode_OSM("Carmen Park, Boston", as.sf=T) 

leaflet() %>% addTiles() %>% addCircleMarkers(data=boston_cbd,col="red",weight=3)

## Chicago CBD
chicago_cbd = geocode_OSM("233 S Wacker Dr, Chicago, IL 60606, Estados Unidos", as.sf=T) ## Willis Tower

leaflet() %>% addTiles() %>% addCircleMarkers(data=chicago_cbd,col="red",weight=3)

## create distances Boston
boston_cbd = boston_cbd %>% st_transform(st_crs(boston)) ## change CRS

boston$dist_CBD = st_distance(boston,boston_cbd) 

boston$dist_CBD = as.numeric(boston$dist_CBD)*0.000621371

## create distances Chicago
chicago_cbd = chicago_cbd %>% st_transform(st_crs(chicago)) ## change CRS

chicago$dist_CBD = st_distance(chicago,chicago_cbd) 

chicago$dist_CBD = as.numeric(chicago$dist_CBD)*0.000621371

## prepare data
boston$City="Boston" ## create name city

chicago$City="Chicago" ## create name city

chicago=chicago %>% filter(dist_CBD<=10) ## keep block groups in Cook County that are within 10 miles of the city center
st_geometry(chicago) = NULL
st_geometry(boston) = NULL

dta = rbind(chicago,boston) # stack data
```
  
```{r}
## scaterplot
ggplot(dta, aes(x=dist_CBD, y=estimate, color=City)) +
geom_point(shape=1) + geom_smooth(method="lm") + xlab("Distance to CBD (miles)") +
ylab("Median Housing Prices ($)") + theme_test()
```

#### **4.3 Bogotá**

```{r,warning=F}
## leer conjutno de datos
bog  <- import("input/block_median_housing_value_bog.rds")
```

```{r,warning=F}
## ggplot + mapping
p = ggplot(data=bog) + geom_sf(mapping = aes(fill=q_price) , size=0.3 , col=NA)  +
    scale_fill_manual(values=c("yellow","#FF9900","#FF6600","#CC0000","#990000"),
                      name="Valor mediano\n del metro cuadrado") 

## add theme and labels
p = p + theme_bw() + labs(x="",y="")

## add scalebar and north symbol
p = p + north(data=bog , location="topleft") + 
    scalebar(data=bog , dist=5 , dist_unit="km" , transform=T , model="WGS84")
p
```


```{r,warning=F}
## plot 
ggplot(bog, aes(x=dist_CBD/1000, y=price_surface_median/1000000)) +
geom_point(shape=1) + geom_smooth(method="lm") + 
theme_bw() + labs(x="Distancía al centro de negocios de la ciudad (kilómetros)",
                  y="Valor mediano del metro cuadrado (millones)") 
```
